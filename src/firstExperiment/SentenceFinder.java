/**
 * Created by Or Elmaliach on 05/12/2014
 */

package firstExperiment;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class SentenceFinder {

    public static void main(String args[]){
        HashMap<String, Double> hashTable = analyzeFolder("test");
        return;
    }

    public static HashMap<String, Double> analyzeFolder(String path){
        HashMap<String, Double> hashTable = new HashMap<String, Double>();  //ATTENTION: the value of an entry will be set to 0 if the corresponding
                                                                            //sentence is not according to rules and needs to be removed in a later iteration
        File directory = new File(path);

        File[] allFiles = directory.listFiles();
        if (allFiles != null) {
            for (File f : allFiles) {
                parseFile(f, hashTable);
            }
        }
        return filterHashtable(hashTable, 0.5);
    }

    private static HashMap<String, Double> filterHashtable(HashMap<String, Double> hashTable, double limit) {
        Iterator<String> iter = hashTable.keySet().iterator();
        while (iter.hasNext()){
            String sentence = iter.next();
            if(Math.abs(hashTable.get(sentence)) < limit){
                iter.remove();
            }
        }
        return hashTable;
    }

    private static void parseFile(File f, HashMap<String, Double> hashTable){
        try {
            FileReader reader = new FileReader(f.getPath());
            BufferedReader bf = new BufferedReader(reader);
            String line;
            while ((line = bf.readLine()) != null) {
                parseLine(line, hashTable);
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    private static void parseLine(String line, HashMap<String, Double> hashTable){
        double normalizedScore = parseAdjective(line);
        double scoreSum = normalizedScore;
        int scoreCount = 1;

        int delimIndex;
        while((delimIndex = line.indexOf("@@@")) != -1){
            line = line.substring(delimIndex + 3);

            normalizedScore = parseAdjective(line);
            if(normalizedScore * scoreSum < 0){ //opposite polarities
                scoreSum = 0;   //we will delete this sentence later, we need this flag for now so we don't add this sentence later
                break;
            }
            scoreSum += normalizedScore;
            scoreCount++;
        }

        int tabIndex = line.lastIndexOf("\t");
        String sentence = line.substring(tabIndex + 1);

        //if we've reached this point then all of the adjectives for this line's aspect have the same polarity
        double averageScore = scoreSum / scoreCount;
        /* if(Math.abs(averageScore) < 0.5){
            return; //this sentence's opinion is not strong enough
        }*/

        Double pastScore = hashTable.get(sentence);
        if(pastScore == null){
            hashTable.put(sentence, averageScore);   //we store the sentence with the score we found
        }
        else{   //the hashtable already contains this sentence
            if(pastScore * averageScore <= 0){  //we require a sentence with either positive or negative opinion for ALL aspects
                hashTable.put(sentence, 0.0);
            }
            else{
                hashTable.put(sentence, (pastScore + averageScore) / 2);    //TODO: this isn't the most accurate thing (an average of averages)
            }
        }
    }

    private static double parseAdjective(String line){
        int commaIndex = line.indexOf(",");
        line = line.substring(commaIndex + 1);
        commaIndex = line.indexOf(",");
        //String isNegated = line.substring(0, commaIndex);
        line = line.substring(commaIndex + 1);
        commaIndex = line.indexOf(",");
        return (2 * Double.parseDouble(line.substring(0, commaIndex))) - 1; //normalizing the value to the range [-1, 1]
    }
}
